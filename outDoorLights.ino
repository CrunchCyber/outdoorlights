#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>
#include <NTPtimeESP.h>

NTPtime NTPpl("0.pl.pool.ntp.org");
strDateTime dateTime;
ESP8266WebServer server;

char* ssid = "dork";
char* password = "";

int srTurnOn;
int srTurnOff;
int ssTurnOn;
int ssTurnOff;


uint8_t relayPin = 0;
bool turnedOn = false;
bool timeInterupt = false;

void setup()
{
  pinMode(relayPin, OUTPUT);
  digitalWrite(relayPin, LOW);

  WiFi.begin(ssid,password);
  Serial.begin(115200);
  while(WiFi.status()!=WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());

  server.on("/",[](){server.send(200,"text/plain","Hello World!");});
  server.on("/times",setTimes);
  server.on("/lights",lights);
  server.on("/lights/on", lightsOn);
  server.on("/lights/off", lightsOFF);
  server.begin();
}

void loop()
{
  server.handleClient();

  dateTime = NTPpl.getNTPtime(1.0, 1);
  String timeString = "";
  if(dateTime.valid){
    timeString = (String) dateTime.hour + ":" + dateTime.minute;
    checkTime(dateTime.epochTime);
  }
}

void setTimes()
{
  String rxData = server.arg("plain");
  StaticJsonDocument<256> doc;
  deserializeJson(doc, rxData);
  srTurnOn = doc["sr_turn_on"];
  srTurnOff = doc["sr_turn_off"];
  ssTurnOn = doc["ss_turn_on"];
  ssTurnOff = doc["ss_turn_off"];
  server.send(200,"");
}

void checkTime(int epochTime){
  if(epochTime > ssTurnOff && epochTime < srTurnOn && turnedOn){
    digitalWrite(relayPin, LOW);
    Serial.println("Turning SS lights off");
    turnedOn = false;
  } else if(epochTime > srTurnOn && epochTime < srTurnOff && !turnedOn){
    digitalWrite(relayPin, HIGH);
    Serial.println("Turning SR lights on");
    turnedOn = true;
  } else if(epochTime > srTurnOff && epochTime < ssTurnOn && turnedOn){
    digitalWrite(relayPin, LOW);
    Serial.println("Turning SR lights off");
    turnedOn = false;
  } else if(epochTime > ssTurnOn && !turnedOn && epochTime < 2000000000){
    digitalWrite(relayPin, HIGH);
    Serial.println("Turning SS lights on");
    turnedOn = true;
  }
}

void lights()
{
  server.send(418,"");
}

void lightsOn(){
  if(!turnedOn){
    digitalWrite(relayPin, LOW);
  }
   server.sendHeader("Location", String("/lights"), true);
 server.send ( 302, "text/plain", "");
}

void lightsOFF(){
  if(turnedOn){
    digitalWrite(relayPin, HIGH);
  }
     server.sendHeader("Location", String("/lights"), true);
 server.send ( 302, "text/plain", "");
}