#!/usr/bin/python3
import logging
import json
import time
import urllib.request
from datetime import datetime
from datetime import timedelta

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s:%(name)s:%(levelname)s:%(message)s')
file_handler = logging.FileHandler('log/main.log')
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)

def main():
    tx_data = {'sr_turn_on': '', 'sr_turn_off': '', 'ss_turn_on': '', 'ss_turn_off': ''}
    api_url = "https://api.sunrise-sunset.org/json?lat=53.353036&lng=20.976956&date=tomorrow&formatted=0"
    relay_server = "http://192.168.1.48/times"
    with urllib.request.urlopen(api_url) as rx:
        rx_data = json.load(rx)
    times = convert_time_to_local([rx_data['results']['sunrise'], rx_data['results']['sunset']])
    times = adjust_time_to_full_minute(times)
    tx_data = set_times(tx_data, times)
    send_data(tx_data, relay_server)


def convert_time_to_local(times):
    result = []
    time_format = "%Y-%m-%dT%H:%M:%S%z"
    for t in times:
        t = datetime.strptime(t, time_format)
        ts = datetime.timestamp(t) + 7200
        result.append(ts)
    return result


def set_times(tx_data, times):
    tx_data['sr_turn_on'] = str(int(times[0]-1800))
    tx_data['sr_turn_off'] = str(int(times[0]+1800))
    tx_data['ss_turn_on'] = str(int(times[1]-1800))
    tx_data['ss_turn_off'] = calc_ss_turn_off(times)
    return json.dumps(tx_data)


def send_data(tx_data, relay_server):
    req = urllib.request.Request(relay_server)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    json_data_bytes = tx_data.encode('utf-8')
    response = urllib.request.urlopen(req, json_data_bytes)
    if response.getcode() is not '200':
        time.sleep(10)
    logger.info(response.getcode())
    logger.info(tx_data)


def calc_ss_turn_off(times):
    times[0] = datetime.fromtimestamp(times[0])
    times[0] = times[0] - timedelta(hours=(times[0].hour-1), minutes=times[0].minute, seconds=times[0].second)
    time = datetime.timestamp(times[0])
    return str(int(time + 3600 + 1800))


def adjust_time_to_full_minute(times):
    for i in range(len(times)):
        times[i] = datetime.fromtimestamp(times[i])
        times[i] = times[i] - timedelta(seconds=times[i].second)
        times[i] = datetime.timestamp(times[i])
    return times


if __name__ == "__main__":
    main()
